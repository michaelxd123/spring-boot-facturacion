package com.ymt.datajpa.dao;

import com.ymt.datajpa.models.Factura;
import org.springframework.data.repository.CrudRepository;

public interface IFacturaDao extends CrudRepository<Factura, Long> {

}
